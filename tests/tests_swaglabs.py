"""
1.log in:
    1. go to https://www.saucedemo.com/inventory.html
    2. enter username 'standard_user' and password 'secret_sauce' and press the 'LOGIN' button
    3. make sure you're redirected to 'PRODUCTS' page

2.log out:
    1. go to https://www.saucedemo.com/inventory.html
    2. enter username 'standard_user' and password 'secret_sauce' and press the 'LOGIN' button
    3. make sure you're redirected to 'PRODUCTS' page
    4. press the 'menu' button
    5. choose and press 'LOGOUT'
    6. make sure you've been logged out (redirected to login page)

3.ascending sorting by price
    1. go to https://www.saucedemo.com/inventory.html
    2. enter username 'standard_user' and password 'secret_sauce' and press the 'LOGIN' button
    3. make sure you're redirected to 'PRODUCTS' page
    4. press sorting button
    5. choose and press 'Price (low to high)'
    6. make sure that the item with the lowest price is the first in the list

4.add to cart
    1. go to https://www.saucedemo.com/inventory.html
    2. enter username 'standard_user' and password 'secret_sauce' and press the 'LOGIN' button
    3. make sure you're redirected to 'PRODUCTS' page
    4. choose the first element and add it to cart
    5. go to cart page
    6. make sure you're redirected to cart page
    7. make sure the chosen element is present

5. remove from cart
    1. go to https://www.saucedemo.com/inventory.html
    2. enter username 'standard_user' and password 'secret_sauce' and press the 'LOGIN' button
    3. make sure you're redirected to 'PRODUCTS' page
    4. choose the first element and add it to cart
    5. go to cart page
    6. make sure you're redirected to cart page
    7. make sure the chosen element is present
    8. click the 'remove' button
    9. make sure the cart is empty

6. checkout
    1. go to https://www.saucedemo.com/inventory.html
    2. enter username 'standard_user' and password 'secret_sauce' and press the 'LOGIN' button
    3. make sure you're redirected to 'PRODUCTS' page
    4. choose the first element and add it to cart
    5. go to cart page
    6. make sure you're redirected to 'CART' page
    7. make sure the chosen element is present
    8. press the 'CHECKOUT' button
    9. make sure you're redirected to 'CHECKOUT_INFORMATION' page
    10. enter first name, last name and zip code and press 'CONTINUE'
    11. make sure you're redirected to 'CHECKOUT_OVERVIEW' page
    12. make sure you're paying for the selected item
    13. press 'FINISH'
    14 make sure you're redirected to 'CHECKOUT_COMPLETE' page
    15. press 'BACK HOME'
"""

headline_1 = 'Products'
headline_2 = 'Your Cart'
headline_3 = 'Checkout: Your Information'
headline_4 = 'Checkout: Overview'
headline_5 = 'Checkout: Complete!'
min_price = '$7.99'
item_in_cart_1 = 'Sauce Labs Backpack'

from test_website.pages.login_page import LoginPage
from test_website.pages.products_page import ProductsPage
from test_website.pages.cart_page import CartPage
from test_website.pages.checkout_page import CheckoutPage
from time import sleep


def test_log_in(driver):
    driver.get('https://www.saucedemo.com/inventory.html')
    login_page = LoginPage(driver)
    login_page.enter_username('standard_user')
    login_page.enter_password('secret_sauce')
    login_page.click_on_login_button()
    products_page = ProductsPage(driver)
    products_page_headline = products_page.get_headline_text()
    assert headline_1.lower() in products_page_headline.lower()


def test_log_out(driver):
    driver.get('https://www.saucedemo.com/inventory.html')
    login_page = LoginPage(driver)
    login_page.enter_username('standard_user')
    login_page.enter_password('secret_sauce')
    login_page.click_on_login_button()
    products_page = ProductsPage(driver)
    products_page_headline = products_page.get_headline_text()
    assert headline_1.lower() in products_page_headline.lower()
    products_page.press_menu_button()
    sleep(0.5)
    products_page.click_on_log_out()
    assert login_page.login_page_check()


def test_sort_by_price_low_to_high(driver):
    driver.get('https://www.saucedemo.com/inventory.html')
    login_page = LoginPage(driver)
    login_page.enter_username('standard_user')
    login_page.enter_password('secret_sauce')
    login_page.click_on_login_button()
    products_page = ProductsPage(driver)
    products_page_headline = products_page.get_headline_text()
    assert headline_1.lower() in products_page_headline.lower()
    products_page.ascending_sorting_by_price()
    ascending_price_list = products_page.check_ascending_sorting()

    assert ascending_price_list == min_price


def test_add_to_cart(driver):
    driver.get('https://www.saucedemo.com/inventory.html')
    login_page = LoginPage(driver)
    login_page.enter_username('standard_user')
    login_page.enter_password('secret_sauce')
    login_page.click_on_login_button()
    products_page = ProductsPage(driver)
    products_page_headline = products_page.get_headline_text()
    assert headline_1.lower() in products_page_headline.lower()
    products_page.add_to_cart()
    products_page.switch_to_cart()
    cart_page = CartPage(driver)
    cart_page_headline = cart_page.get_headline_text()
    assert headline_2.lower() in cart_page_headline.lower()
    item_in_cart = cart_page.get_item_title()
    assert item_in_cart_1.lower() in item_in_cart.lower()


def test_remove_from_cart(driver):
    driver.get('https://www.saucedemo.com/inventory.html')
    login_page = LoginPage(driver)
    login_page.enter_username('standard_user')
    login_page.enter_password('secret_sauce')
    login_page.click_on_login_button()
    products_page = ProductsPage(driver)
    products_page_headline = products_page.get_headline_text()
    assert headline_1.lower() in products_page_headline.lower()
    products_page.add_to_cart()
    products_page.switch_to_cart()
    cart_page = CartPage(driver)
    cart_page_headline = cart_page.get_headline_text()
    assert headline_2.lower() in cart_page_headline.lower()
    item_in_cart = cart_page.get_item_title()
    assert item_in_cart_1.lower() in item_in_cart.lower()
    cart_page.remove_item_from_cart()
    cart_page.check_empty_cart()


def test_checkout(driver):
    driver.get('https://www.saucedemo.com/inventory.html')
    login_page = LoginPage(driver)
    login_page.enter_username('standard_user')
    login_page.enter_password('secret_sauce')
    login_page.click_on_login_button()
    products_page = ProductsPage(driver)
    products_page_headline = products_page.get_headline_text()
    assert headline_1.lower() in products_page_headline.lower()
    products_page.add_to_cart()
    products_page.switch_to_cart()
    cart_page = CartPage(driver)
    cart_page_headline = cart_page.get_headline_text()
    assert headline_2.lower() in cart_page_headline.lower()
    item_in_cart = cart_page.get_item_title()
    assert item_in_cart_1.lower() in item_in_cart.lower()
    cart_page.press_checkout_button()
    checkout_page = CheckoutPage(driver)
    checkout_page_headline = checkout_page.get_headline_text()
    assert headline_3.lower() in checkout_page_headline.lower()
    checkout_page.enter_first_name('Inna')
    checkout_page.enter_last_name('Apanovich')
    checkout_page.enter_zip_code('220030')
    checkout_page.press_continue_button()
    checkout_page_headline = checkout_page.get_headline_text()
    assert headline_4.lower() in checkout_page_headline.lower()
    item_to_buy = checkout_page.item_to_pay_for()
    assert item_in_cart_1.lower() in item_to_buy.lower()
    checkout_page.press_finish_button()
    checkout_page_headline = checkout_page.get_headline_text()
    assert headline_5.lower() in checkout_page_headline.lower()
    checkout_page.press_back_home_button()
