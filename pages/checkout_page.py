import pytest
from test_website.base_page import BasePage
from test_website.locators import checkout_page_locators

class CheckoutPage(BasePage):

    def get_headline_text(self) -> str:
        element = self.find_element(checkout_page_locators.CHECKOUT_PAGE_HEADLINE)
        return element.text

    def enter_first_name(self, text):
        self.find_element(checkout_page_locators.FIRST_NAME).send_keys(text)

    def enter_last_name(self, text):
        self.find_element(checkout_page_locators.LAST_NAME).send_keys(text)

    def enter_zip_code(self, text):
        self.find_element(checkout_page_locators.ZIP_CODE).send_keys(text)

    def press_continue_button(self):
        self.find_element(checkout_page_locators.CONTINUE_BUTTON).click()

    def item_to_pay_for(self):
        element = self.find_element(checkout_page_locators.ITEM_TO_BUY)
        return element.text

    def press_finish_button(self):
        self.find_element(checkout_page_locators.FINISH_BUTTON).click()

    def press_back_home_button(self):
        self.find_element(checkout_page_locators.BACK_HOME_BUTTON).click()


