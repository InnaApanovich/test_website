from selenium.common.exceptions import TimeoutException
from test_website.base_page import BasePage
from test_website.locators import cart_page_locators


class CartPage(BasePage):

    def get_headline_text(self) -> str:
        element = self.find_element(cart_page_locators.CART_PAGE_HEADLINE)
        return element.text

    def get_item_title(self):
        element = self.find_element(cart_page_locators.ITEM_IN_CART)
        return element.text

    def remove_item_from_cart(self):
        self.find_element(cart_page_locators.REMOVE_FROM_CART_BUTTON).click()

    def check_empty_cart(self):
        try:
            self.find_element(cart_page_locators.ITEM_IN_CART, time=1)
            return False
        except TimeoutException:
            return True

    def press_checkout_button(self):
        self.find_element(cart_page_locators.CHECKOUT_BUTTON).click()
