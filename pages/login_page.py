from selenium.common.exceptions import TimeoutException
from test_website.base_page import BasePage
from test_website.locators import login_page_locators

class LoginPage(BasePage):

    def enter_username(self, text):
        self.find_element(login_page_locators.USERNAME).send_keys(text)

    def enter_password(self, text):
        self.find_element(login_page_locators.PASSWORD).send_keys(text)

    def click_on_login_button(self):
        self.find_element(login_page_locators.LOGIN_BUTTON).click()

    def login_page_check(self):
        try:
            self.find_element(login_page_locators.SWAG_BOT, time=1)
            return True
        except TimeoutException:
            return False