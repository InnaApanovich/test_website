from test_website.base_page import BasePage
from test_website.locators import products_page_locators

class ProductsPage(BasePage):

    def get_headline_text(self) -> str:
        element = self.find_element(products_page_locators.PRODUCTS_PAGE_HEADLINE)
        return element.text

    def press_menu_button(self):
        self.find_element(products_page_locators.MENU_BUTTON).click()

    def click_on_log_out(self):
        self.find_element(products_page_locators.LOG_OUT).click()

    def ascending_sorting_by_price(self):
        self.find_element(products_page_locators.SORTING_BUTTON).click()
        self.find_element(products_page_locators.ASCENDING_SORTING).click()

    def check_ascending_sorting(self):
        product_price_list = self.find_elements(products_page_locators.PRODUCT_PRICE)
        return product_price_list[0].text

    def add_to_cart(self):
        self.find_element(products_page_locators.ADD_TO_CART_BUTTON).click()

    def switch_to_cart(self):
        self.find_element(products_page_locators.GO_TO_CART_BUTTON).click()