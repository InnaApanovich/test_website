from selenium.webdriver.common.by import By

USERNAME = (By.XPATH, '//input[@placeholder="Username"]')

PASSWORD = (By.XPATH, '//input[@placeholder="Password"]')

LOGIN_BUTTON = (By.XPATH, '//input[@type="submit"]')

SWAG_BOT = (By.XPATH, '//div[@class="bot_column"]')
