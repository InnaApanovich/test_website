from selenium.webdriver.common.by import By

CART_PAGE_HEADLINE = (By.XPATH, '//span[@class="title"]')

ITEM_IN_CART = (By.XPATH, '//div[@class="inventory_item_name"]')

REMOVE_FROM_CART_BUTTON = (By.XPATH, '//button[@id="remove-sauce-labs-backpack"]')

CHECKOUT_BUTTON = (By.XPATH, '//button[@id="checkout"]')