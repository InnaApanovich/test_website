from selenium.webdriver.common.by import By

CHECKOUT_PAGE_HEADLINE = (By.XPATH, '//span[@class="title"]')

FIRST_NAME = (By.XPATH, '//input[@id="first-name"]')

LAST_NAME = (By.XPATH, '//input[@id="last-name"]')

ZIP_CODE = (By.XPATH, '//input[@id="postal-code"]')

CONTINUE_BUTTON = (By.XPATH, '//input[@id="continue"]')

ITEM_TO_BUY = (By.XPATH, '//div[@class="inventory_item_name"]')

FINISH_BUTTON = (By.XPATH, '//button[@id="finish"]')

BACK_HOME_BUTTON = (By.XPATH, '//button[@id="back-to-products"]')