from selenium.webdriver.common.by import By

PRODUCTS_PAGE_HEADLINE = (By.XPATH, '//span[@class="title"]')

MENU_BUTTON = (By.CSS_SELECTOR, '#react-burger-menu-btn')

LOG_OUT = (By.CSS_SELECTOR, '#logout_sidebar_link')

ADD_TO_CART_BUTTON = (By.XPATH, '//button[@class="btn btn_primary btn_small btn_inventory"]')

GO_TO_CART_BUTTON = (By.XPATH, '//a[@class="shopping_cart_link"]')

SORTING_BUTTON = (By.XPATH, '//select[@class="product_sort_container"]')

ASCENDING_SORTING = (By.XPATH, '//option[text()="Price (low to high)"]')

PRODUCT_PRICE = (By.XPATH, '//div[@class="inventory_item_price"]')